<?php declare(strict_types=1);

namespace App\DependencyInjection;

use App\Base\Admin\AdminRegistry;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class AdminCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition(
            AdminRegistry::class
        );
        $services = $container->findTaggedServiceIds('admin.controller');
        foreach ($services as $id => $config) {
            $definition->addMethodCall('add', [
                new Reference($id)
            ]);
        }
    }
}
