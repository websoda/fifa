<?php declare(strict_types=1);

namespace App\DependencyInjection;

use App\Base\Service\ImageSize;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Yaml;

class ImageSizesCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $config = Yaml::parse(
            file_get_contents(__DIR__.'/../../config/image_sizes.yaml')
        );
        $definition = $container->getDefinition(
            ImageSize::class
        );
        $definition->addMethodCall('setSizes', [
            $config['sizes']
        ]);
    }
}
