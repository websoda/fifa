<?php

namespace App\Form\Request;

use App\Traits\Form\UniqueIdFormTrait;
use App\Entity\Request\Recall;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecallType extends AbstractType
{
    // Для того чтобы можно было корректно выводить несколько форм на страницу и идентификаторы не пересекались
    use UniqueIdFormTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('phone')
            ->add('submit' , SubmitType::class, [
                'label' => 'Отправить заявку'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recall::class,
            'unique_id' => false
        ]);
    }
}
