<?php declare(strict_types=1);

namespace App\Controller\Base;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseController extends AbstractController
{

}