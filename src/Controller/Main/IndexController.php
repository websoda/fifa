<?php declare(strict_types=1);

namespace App\Controller\Main;

use App\Controller\Base\BaseController;
use App\Base\Service\Breadcrumbs;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends BaseController
{
    /**
     * @Route(name="main:index", path="/")
     *
     * @param Request $request
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function index(Request $request, Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->add('Index page', 'main:index');

        $this->addFlash('error', 'Error');
        $this->addFlash('success', 'Success');

        return $this->render('pages/index/index.html.twig', [

        ]);
    }
}