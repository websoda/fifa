<?php declare(strict_types=1);

namespace App\Controller\Games;

use App\Controller\Base\BaseController;
use App\Entity\Games\Game;
use App\Repository\Games\GameRepository;
use App\Base\Service\Breadcrumbs;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends BaseController
{
    /**
     * @Route(name="main:games", path="/games")
     *
     * @param Request $request
     * @param Breadcrumbs $breadcrumbs
     * @param GameRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, Breadcrumbs $breadcrumbs, GameRepository $repository, PaginatorInterface $paginator): Response
    {
        $breadcrumbs->add('Игры');

        $pager = $paginator->paginate(
            $repository->getAllQuery(),
            $request->query->getInt('page', 1),
            1
        );

        return $this->render('pages/games/index.html.twig', [
            'pager' => $pager
        ]);
    }

    /**
     * @Route(name="main:game", path="/games/{slug}")
     * @param Game $game
     * @param Breadcrumbs $breadcrumbs
     * @return Response
     */
    public function view(Game $game, Breadcrumbs $breadcrumbs): Response
    {
        $breadcrumbs->add('Игры', 'main:games', []);
        $breadcrumbs->add($game->getName());

        return $this->render('pages/game/index.html.twig', [
            'game' => $game
        ]);
    }
}