<?php declare(strict_types=1);

namespace App\Controller\Request;

use App\Controller\Base\BaseController;
use App\Form\Request\RecallType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RequestController extends BaseController
{
    /**
     * @Route(name="request:recall", path="/request/recall")
     * @param Request $request
     * @return JsonResponse
     */
    public function recall(Request $request)
    {
        $form = $this->createForm(RecallType::class);
        $form->handleRequest($request);
        return $this->processInlineForm($form);
    }

    public function processInlineForm(FormInterface $form)
    {
        if (!$form->isSubmitted()) {
            throw new BadRequestException("Form are not submitted");
        }

        if (!$form->isValid()) {
            $errors = [];
            /** @var Form $child */
            foreach ($form as $child) {
                if (!$child->isValid()) {
                    foreach ($child->getErrors() as $error) {
                        $errors[$child->getName()][] = $error->getMessage();
                    }
                }
            }
            $data = [
                'state' => 'error',
                'errors' => [
                    $form->getName() => $errors
                ]
            ];
        } else {
            $data = [
                'state' => 'success'
            ];

            $em = $this->getEntityManager();

            $em->persist($form->getData());
            $em->flush();
        }
        return new JsonResponse($data);
    }
}