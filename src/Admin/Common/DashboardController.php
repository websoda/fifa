<?php

namespace App\Admin\Common;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin:index")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Admin');
    }

    public function configureAssets(): Assets
    {
        $assets = parent::configureAssets();
        $assets->addJsFile('static_admin/ui-custom/jquery-ui.min.js');
        $assets->addCssFile('static_admin/ui-custom/jquery-ui.min.css');
        $assets->addCssFile('static_admin/admin.css');
        $assets->addJsFile('static_admin/sort.js');
        return $assets;
    }

    public function configureMenuItems(): iterable
    {
        /**
         * Иконки можно поискать вот тута:
         * @link https://fontawesome.com/icons?d=gallery&p=2&m=free
         */
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');

//        yield MenuItem::section('Заявки', 'fa fa-bell-o');
//        yield MenuItem::linkToCrud('Recalls', 'fa fa-envelope-o', Recall::class);
//
//        yield MenuItem::section('Видеоигры', 'fa fa-ghost');
//        yield MenuItem::linkToCrud('Игры', 'fa fa-gamepad', Game::class);
//        yield MenuItem::linkToCrud('Отзывы', 'far fa-comment-alt', GameReview::class);
//
//        yield MenuItem::section('Управление', 'fa fa-wrench');
//        yield MenuItem::linkToCrud('Users', 'fa fa-user-o', User::class)
//            ->setPermission('ROLE_ADMIN');
//        yield MenuItem::linkToCrud('Settings', 'fa fa-cog', Settings::class)
//            ->setAction(Action::EDIT)
//            ->setEntityId(1);
    }

    public function configureCrud(): Crud
    {
        $crud = parent::configureCrud();
        $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig');
        return $crud;
    }
}
