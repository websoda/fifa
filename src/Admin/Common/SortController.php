<?php declare(strict_types=1);

namespace App\Admin\Common;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class SortController extends AbstractController
{
    /**
     * @Route(name="admin_sort", path="/admin/sort")
     */
    public function sort(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $newPosition = 0;
        $currPosition = $request->query->get('currPosition');
        $nextPosition = $request->query->get('nextPosition');
        $prevPosition = $request->query->get('prevPosition');
        $entityFqcn = $request->query->get('fqcn');
        $field = $request->query->get('field');
        $pk = $request->query->get('pk');

        if (!$entityFqcn || !$pk) {
            throw new NotFoundHttpException('Could not get entity information');
        }

        $repo = $entityManager->getRepository($entityFqcn);
        $entity = $repo->find($pk);

        if (!$entity) {
            throw new NotFoundHttpException('Could not fetch entity');
        }

        $accessor = new PropertyAccessor();
        if (!$accessor->isWritable($entity, $field)) {
            throw new AccessDeniedException('Not writable sort field');
        }

        if (is_numeric($prevPosition)) {
            if ($currPosition > $prevPosition) {
                $newPosition = $prevPosition + 1;
            } else {
                $newPosition = $prevPosition;
            }
        } elseif (is_numeric($nextPosition)) {
            if ($nextPosition == 0) {
                $newPosition = 0;
            } else {
                $newPosition = $nextPosition - 1;
            }
        }

        $accessor->setValue($entity, $field, $newPosition);
        $entityManager->persist($entity);
        $entityManager->flush();

        return $this->json([
            'success' => true
        ]);
    }
}
