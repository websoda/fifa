<?php declare(strict_types=1);

namespace App\Traits\Form;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

/**
 * Trait UniqueIdFormTrait
 * Applicable to Type
 *
 * @package App\Base\Form
 */
trait UniqueIdFormTrait
{
    /**
     * Для того чтобы можно было корректно выводить несколько форм на страницу и идентификаторы не пересекались
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (!isset($options['unique_id']) || $options['unique_id']) {
            $view->vars['id'] = uniqid("", false) . '_' . $view->vars['id'];
        }
        parent::buildView($view, $form, $options);
    }
}
