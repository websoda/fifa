<?php

namespace App\Entity\Games;

use App\Base\Annotation\Orm\HtmlEditable;
use App\Repository\Games\GameRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Sluggable\Sluggable;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 * @Vich\Uploadable()
 */
class Game implements Sluggable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name = '';

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    private $slug;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * Информация о размерах изображений хранится в config/image_sizes.yaml
     *
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="image")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $image;

    /**
     * Required for image uploading
     *
     * @ORM\Column(type="datetime")
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @HtmlEditable()
     */
    private $text;

    /**
     * @ORM\OneToMany(targetEntity=GameLevel::class, mappedBy="game", orphanRemoval=true)
     */
    private $gameLevels;

    /**
     * @ORM\OneToMany(targetEntity=GameReview::class, mappedBy="game")
     */
    private $gameReviews;

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
        $this->gameLevels = new ArrayCollection();
        $this->gameReviews = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @param string $text
     * @return Game
     */
    public function setText(?string $text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     * @throws \Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;
        if (null !== $imageFile) {
            $this->updatedAt = new DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug(?string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Collection|GameLevel[]
     */
    public function getGameLevels(): Collection
    {
        return $this->gameLevels;
    }

    public function addGameLevel(GameLevel $gameLevel): self
    {
        if (!$this->gameLevels->contains($gameLevel)) {
            $this->gameLevels[] = $gameLevel;
            $gameLevel->setGame($this);
        }

        return $this;
    }

    public function removeGameLevel(GameLevel $gameLevel): self
    {
        if ($this->gameLevels->removeElement($gameLevel)) {
            // set the owning side to null (unless already changed)
            if ($gameLevel->getGame() === $this) {
                $gameLevel->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GameReview[]
     */
    public function getGameReviews(): Collection
    {
        return $this->gameReviews;
    }

    public function addGameReview(GameReview $gameReview): self
    {
        if (!$this->gameReviews->contains($gameReview)) {
            $this->gameReviews[] = $gameReview;
            $gameReview->setGame($this);
        }

        return $this;
    }

    public function removeGameReview(GameReview $gameReview): self
    {
        if ($this->gameReviews->removeElement($gameReview)) {
            // set the owning side to null (unless already changed)
            if ($gameReview->getGame() === $this) {
                $gameReview->setGame(null);
            }
        }

        return $this;
    }
}
