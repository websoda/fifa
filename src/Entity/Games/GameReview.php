<?php

namespace App\Entity\Games;

use App\Base\Annotation\Orm\Help;
use App\Base\Annotation\Orm\Label;
use App\Base\Annotation\Orm\ManyToOneAdmin;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class GameReview
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Label(text="Текст отзыва")
     * @Help(text="Не более 20 слов")
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="gameReviews")
     * @ManyToOneAdmin(autocomplete=false)
     * @ORM\JoinColumn(nullable=false)
     * @Label(text="Игра")
     * @Help(text="Текст для помощи")
     */
    private $game;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }
}
