<?php

namespace App\Entity\Main;

use App\Repository\Main\SettingsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SettingsRepository::class)
 */
class Settings
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $address_lat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $address_lng;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $policy;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $counters_body;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $counters_head;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddressLat(): ?float
    {
        return $this->address_lat;
    }

    public function setAddressLat(?float $address_lat): self
    {
        $this->address_lat = $address_lat;

        return $this;
    }

    public function getAddressLng(): ?float
    {
        return $this->address_lng;
    }

    public function setAddressLng(?float $address_lng): self
    {
        $this->address_lng = $address_lng;

        return $this;
    }

    public function getPolicy(): ?string
    {
        return $this->policy;
    }

    public function setPolicy(?string $policy): self
    {
        $this->policy = $policy;

        return $this;
    }

    public function getCountersBody(): ?string
    {
        return $this->counters_body;
    }

    public function setCountersBody(?string $counters_body): self
    {
        $this->counters_body = $counters_body;

        return $this;
    }

    public function getCountersHead(): ?string
    {
        return $this->counters_head;
    }

    public function setCountersHead(?string $counters_head): self
    {
        $this->counters_head = $counters_head;

        return $this;
    }
}
