# Библиотеки расширений

## Основные типы расширений

| Наименование | Когда использовать?                       |
| ------ | ----------------------------------------- |
| function | Генерация контента                        |
| filter | Преобразование значения                   |
| test   | Проверка на логическое (boolean) условие  |
| operator | Создание собственного оператора           |

*Подробнее обо всех расширениях в [официальной документации](https://twig.symfony.com/doc/3.x/advanced.html)*

## Добавление собственного расширения

1. По аналогии создайте свою библиотеку с расширениями
2. Ознакомьтесь с уже определенными [расширениями](https://symfony.com/doc/current/reference/twig_reference.html) в симфони
3. Добавьте метод с расширением:

```php
public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
{
    $price = number_format($number, $decimals, $decPoint, $thousandsSep);
    $price = '$'.$price;

    return $price;
}
```

4. Определитесь с типом расширения
5. Добавьте новое расширение в соответсвующие настройки:

```php
public function getFilters()
{
    return [
        new TwigFilter('price', [$this, 'formatPrice']),
    ];
}
```

## Наш вариант (с аннотациями)

См [Гайд](../../docs/GUIDE.md).