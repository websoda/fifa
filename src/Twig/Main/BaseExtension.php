<?php declare(strict_types=1);

namespace App\Twig\Main;

use App\Base\Annotation\Twig\TwigFilter;
use App\Base\Annotation\Twig\TwigFunction;
use App\Base\Object\Breadcrumb;
use App\Base\Twig\AnnotatedExtension;
use App\Entity\Main\Settings;
use App\Repository\Main\SettingsRepository;
use App\Base\Service\Breadcrumbs;
use Twig\Environment;

class BaseExtension extends AnnotatedExtension
{
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;
    /**
     * @var SettingsRepository
     */
    private $settingsRepository;

    public function __construct(Environment $twig, Breadcrumbs $breadcrumbs, SettingsRepository $settingsRepository)
    {
        $this->twig = $twig;
        $this->breadcrumbs = $breadcrumbs;
        $this->settingsRepository = $settingsRepository;
    }

    /**
     * @TwigFunction(name="get_breadcrumbs")
     * @param string $list
     * @return Breadcrumb[]
     */
    public function getBreadcrumbs(string $list = Breadcrumbs::DEFAULT_LIST)
    {
        return $this->breadcrumbs->all($list);
    }

    /**
     * @TwigFunction(name="settings")
     * @return Settings
     */
    public function getSettings(): Settings
    {
        return $this->settingsRepository->getOrCreate();
    }

    /**
     * @TwigFilter(name="tel")
     * @param $phone
     * @return string
     */
    public function tel($phone): string
    {
        $phone = (string) $phone;
        $phone = preg_replace('/[^0-9\+]/','', $phone);
        if (mb_substr($phone, 1, 1, 'UTF-8') === '8') {
            $phone = '+7' . mb_substr($phone, 1, mb_strlen($phone, 'UTF-8') - 1, 'UTF-8');
        }
        return $phone;
    }

    /**
     * @TwigFilter(name="price")
     * @param $number
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     * @return string
     */
    public function formatPrice($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }

    /**
     * @TwigFunction(name="icon")
     * @param string $name
     * @param string $template
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function icon(string $name, string $template = '_base/icon.html.twig')
    {
        return $this->twig->render($template, [
            'name' => $name
        ]);
    }
}