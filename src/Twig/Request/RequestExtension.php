<?php declare(strict_types=1);

namespace App\Twig\Request;

use App\Base\Annotation\Twig\TwigFunction;
use App\Base\Twig\AnnotatedExtension;
use App\Form\Request\RecallType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormView;

class RequestExtension extends AnnotatedExtension
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @TwigFunction(name="get_recall_form")
     * @return FormView
     */
    public function getRecallForm()
    {
        return $this->formFactory->create(RecallType::class)->createView();
    }
}