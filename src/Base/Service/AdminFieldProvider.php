<?php

namespace App\Base\Service;

use App\Base\Admin\AdminRegistry;
use App\Base\Admin\PositionField;
use App\Base\Annotation\Orm\Help;
use App\Base\Annotation\Orm\HtmlEditable;
use App\Base\Annotation\Orm\Label;
use App\Base\Annotation\Orm\ManyToOneAdmin;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Annotation;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToOne;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use EasyCorp\Bundle\EasyAdminBundle\Registry\CrudControllerRegistry;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Gedmo\Mapping\Annotation\SortablePosition;
use Gedmo\Mapping\Annotation\Timestampable;
use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Form\Type\VichImageType;

/**
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class AdminFieldProvider
{
    private $adminContextProvider;

    public function __construct(AdminContextProvider $adminContextProvider)
    {
        $this->adminContextProvider = $adminContextProvider;
    }

    public function getDefaultFields(string $pageName, object $entity): array
    {
        $maxNumProperties = Crud::PAGE_INDEX === $pageName ? 7 : \PHP_INT_MAX;
        $context = $this->adminContextProvider->getContext();
        if (!$context) {
            throw new \RuntimeException('Empty admin context');
        }

//        $excludedPropertyTypes = [
//            Crud::PAGE_EDIT => [Types::BINARY, Types::BLOB, Types::JSON],
//            Crud::PAGE_INDEX => [Types::BINARY, Types::BLOB, Types::JSON],
//            Crud::PAGE_NEW => [Types::BINARY, Types::BLOB, Types::JSON],
//            Crud::PAGE_DETAIL => [Types::BINARY, Types::JSON],
//        ];

        $excludedPropertyNames = [
            Crud::PAGE_EDIT => [],
            Crud::PAGE_INDEX => ['password', 'salt', 'slug', 'updatedAt', 'uuid'],
            Crud::PAGE_NEW => [],
            Crud::PAGE_DETAIL => [],
        ];

        // Дернуть все свойства объекта
        // Проанализировать на их являемость полями -> Для нужных кастануть поля
        $fields = $this->getFields($entity);

        // Отсеять те, которые мы не хотим отображать
        foreach ($fields as $key => $field) {
            $propertyName = $field->getAsDto()->getProperty();
            if (
                \in_array($propertyName, $excludedPropertyNames[$pageName], true)
            ) {
                unset($fields[$key]);
            }
        }

        if (\count($fields) > $maxNumProperties) {
            $fields = \array_slice($fields, 0, $maxNumProperties, true);
        }

        return $fields;
    }

    /**
     * @param object $entity
     * @return Field[]
     * @throws \ReflectionException
     */
    protected function getFields(object $entity): array
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $annotationReader = new AnnotationReader();
        $reflectionClass = new ReflectionClass(get_class($entity));
        $reflectionProperties = $reflectionClass->getProperties();
        $allFields = [];
        foreach ($reflectionProperties as $reflectionProperty) {
            $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);

            // Перед кастом поля - не забываем проверить на то, можем ли мы его читать, если не можем - не кастуем вообще
            if (!$propertyAccessor->isReadable($entity, $reflectionProperty->getName())) {
                continue;
            }

            if ($this->searchAnnotation(Column::class, $propertyAnnotations)) {
                $fields = $this->processColumnProperty(
                    $entity,
                    $reflectionProperty,
                    $propertyAccessor,
                    $annotationReader,
                    $reflectionProperties
                );
            }
            if ($this->searchAnnotation(ManyToOne::class, $propertyAnnotations)) {
                $fields = $this->processManyToOneProperty(
                    $entity,
                    $reflectionProperty,
                    $propertyAccessor,
                    $annotationReader,
                    $reflectionProperties
                );
            }

            if ($fields) {
                $allFields = array_merge($allFields, $fields);
            }
        }
        $allFields = $this->afterFieldsProcessed($allFields);
        return $allFields;
    }

    /**
     * @param Field[] $fields
     * @return array
     */
    protected function afterFieldsProcessed(array $fields): array
    {
        $hasSortableField = false;
        foreach ($fields as $field) {
            if ($field instanceof PositionField) {
                $hasSortableField = true;
            }
        }
        if ($hasSortableField) {
            foreach ($fields as $field) {
                $field->setSortable(false);
            }
        }
        return $fields;
    }

    /**
     * Обрабатываем обычный Column
     *
     * @param object $entity
     * @param PropertyAccessor $propertyAccessor
     * @param array $reflectionProperties
     * @return Field[]
     */
    protected function processColumnProperty(
        object $entity,
        ReflectionProperty $reflectionProperty,
        PropertyAccessor $propertyAccessor,
        AnnotationReader $annotationReader,
        array $reflectionProperties
    ): array {
        $name = $reflectionProperty->getName();
        $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);
        $fields = [];

        if ($this->searchAnnotation(HtmlEditable::class, $propertyAnnotations)) {
            // Если на свойстве висит что оно - HTML-редактируемое, цепляем редактор

            $field = TextEditorField::new($name)
                ->setFormType(CKEditorType::class);
            $this->applyLabelAndHelpToField($field, $propertyAnnotations);
            $fields[] = $field;
        } elseif ($vichProperty = $this->findVichProperty($name, $annotationReader, $reflectionProperties)) {
            // Если есть Vich свойство, которое ссылается на данное поле - кастуем Vich-загрузчик

            $field = Field::new($vichProperty->getName());
            if (strpos($name, 'image') !== false) {
                $field->setFormType(VichImageType::class);
            } else {
                $field->setFormType(VichFileType::class);
            }
            $field->hideOnIndex();
            $this->applyLabelAndHelpToField($field, $propertyAnnotations);
            $fields[] = $field;
        } elseif ($this->searchAnnotation(SortablePosition::class, $propertyAnnotations)) {
            // Если поле сортировки - делаем поле сортировки О_о

            $field = PositionField::new($name);
            $field->hideOnForm();
            $field->hideOnDetail();
            $this->applyLabelAndHelpToField($field, $propertyAnnotations);
            $fields[] = $field;
        } else {
            // В противном случае кастуем обычное поле

            $field = Field::new($name);
            $this->applyLabelAndHelpToField($field, $propertyAnnotations);
            $fields[] = $field;
        }


        foreach ($fields as $field) {
            // После каста поля - не забываем проверить на то, можем ли мы его писать - если не можем, скипаем на формах
            if (!$propertyAccessor->isWritable($entity, $name)) {
                $field->hideOnForm();
            }
            // Timestampable-поля не редактируются по-умолчанию
            if ($this->searchAnnotation(Timestampable::class, $propertyAnnotations)) {
                $field->hideOnForm();
            }
        }

        return $fields;
    }

    /**
     * Обрабатываем обычный Column
     *
     * @param object $entity
     * @param PropertyAccessor $propertyAccessor
     * @param array $reflectionProperties
     * @return Field[]
     */
    protected function processManyToOneProperty(
        object $entity,
        ReflectionProperty $reflectionProperty,
        PropertyAccessor $propertyAccessor,
        AnnotationReader $annotationReader,
        array $reflectionProperties
    ): array {
        $name = $reflectionProperty->getName();
        $fields = [];

        $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);
        /** @var ManyToOne $manyToOneAnnotation */
        $manyToOneAnnotation = $this->searchAnnotation(ManyToOne::class, $propertyAnnotations);
        $entityFqcn = $manyToOneAnnotation->targetEntity;

        $crudController = null;
        if ($context = $this->adminContextProvider->getContext()) {
            $crudController = $context->getCrudControllers()->findCrudFqcnByEntityFqcn($entityFqcn);
        }

        $field = AssociationField::new($name);
        /** @var ManyToOneAdmin $manyToOneAdmin */
        if ($manyToOneAdmin = $this->searchAnnotation(ManyToOneAdmin::class, $propertyAnnotations)) {
            if ($manyToOneAdmin->autocomplete) {
                $field->autocomplete();
            }
            $field->renderAsNativeWidget($manyToOneAdmin->renderAsNativeWidget);
        }
        if ($crudController) {
            $field->setCrudController($crudController);
        }

        $this->applyLabelAndHelpToField($field, $propertyAnnotations);

        $fields[] = $field;


        return $fields;
    }

    /**
     * Применить Label и Help к полю
     *
     * @param FieldInterface $field
     * @param array $propertyAnnotations
     */
    protected function applyLabelAndHelpToField(FieldInterface $field, array $propertyAnnotations): void
    {
        /** @var $label Label */
        if (method_exists($field, 'setLabel') &&
            ($label = $this->searchAnnotation(Label::class, $propertyAnnotations))
        ) {
            $field->setLabel($label->text);
        }

        /** @var $help Help */
        if (method_exists($field, 'setHelp') &&
            ($help = $this->searchAnnotation(Help::class, $propertyAnnotations))
        ) {
            $field->setHelp($help->text);
        }
    }

    /**
     * @param string $annotationClass
     * @param Annotation[] $propertyAnnotations
     * @return object|null
     */
    protected function searchAnnotation(string $annotationClass, array $propertyAnnotations): ?object
    {
        foreach ($propertyAnnotations as $propertyAnnotation) {
            if (is_a($propertyAnnotation, $annotationClass)) {
                return $propertyAnnotation;
            }
        }
        return null;
    }

    /**
     * @param string $pathProperty
     * @param AnnotationReader $annotationReader
     * @param ReflectionProperty[] $reflectionProperties
     * @return ReflectionProperty|null
     * @throws \ReflectionException
     */
    protected function findVichProperty(string $pathProperty, AnnotationReader $annotationReader, array $reflectionProperties): ?ReflectionProperty
    {
        foreach ($reflectionProperties as $reflectionProperty) {
            $annotations = $annotationReader->getPropertyAnnotations($reflectionProperty);
            foreach ($annotations as $annotation) {
                if (
                    $annotation instanceof \Vich\UploaderBundle\Mapping\Annotation\UploadableField &&
                    $annotation->getFileNameProperty() === $pathProperty
                ) {
                    return $reflectionProperty;
                }
            }
        }
        return null;
    }
}
