<?php declare(strict_types=1);

namespace App\Base\Service;

use App\Base\Object\Breadcrumb;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Breadcrumbs
{
    const DEFAULT_LIST = 'DEFAULT';

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var array
     */
    protected $lists = [];

    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param string $name
     * @param string $route
     * @param array $params
     * @param string $list
     */
    public function add(string $name, string $route = '', array $params = [], string $list = self::DEFAULT_LIST): void
    {
        $url = null;
        if ($route !== '') {
            $url = $this->router->generate($route, $params);
        }

        $this->lists[$list][] = new Breadcrumb($name, $url);
    }

    /**
     * @param string $list
     * @return Breadcrumb[]
     */
    public function all(string $list = self::DEFAULT_LIST): array
    {
        return $this->lists[$list] ?? [];
    }
}