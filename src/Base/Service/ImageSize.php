<?php declare(strict_types=1);

namespace App\Base\Service;

class ImageSize
{
    protected $sizes = [];

    public function setSizes(array $sizes)
    {
        $this->sizes = $sizes;
    }

    public function getSizeItems(string $sizeSetName): ?array
    {
        return $this->sizes[$sizeSetName] ?? null;
    }

    /**
     * [
     *     'medium' => LiipConfig,
     *     'medium_2x' => LiipConfig,
     *     'medium_webp' => LiipConfig,
     *     'medium_webp_2x' => LiipConfig
     * ]
     * @param string $sizeSetName
     * @return array
     */
    public function getLiipConfigMap(string $sizeSetName): array
    {
        $result = [];
        $sizes = $this->getSizeItems($sizeSetName);
        if (!$sizes) {
            return [];
        }

        foreach ($sizes as $sizeName => $size) {
            $width = $size['width'] ?? 100;
            $height = $size['height'] ?? 100;
            $mode = $size['size'] ?? 'cover';
            $thumbnailMode = $mode === 'cover' ? 'outbond' : 'inset';
            $filterSet = $size['filter_set'] ?? null;

            $result[$sizeName] = [
                'filter_set' => $filterSet ?: 'default',
                'filters' => [
                    'thumbnail' => [
                        'size' => [$width, $height],
                        'mode' => $thumbnailMode
                    ]
                ]
            ];
            $result["{$sizeName}_webp"] = [
                'filter_set' => $filterSet ?: 'webp',
                'filters' => [
                    'thumbnail' => [
                        'size' => [$width, $height],
                        'mode' => $thumbnailMode
                    ]
                ]
            ];
            $result["{$sizeName}_2x"] = [
                'filter_set' => $filterSet ?: 'default',
                'filters' => [
                    'thumbnail' => [
                        'size' => [$width*2, $height*2],
                        'mode' => $thumbnailMode
                    ]
                ]
            ];
            $result["{$sizeName}_webp_2x"] = [
                'filter_set' => $filterSet ?: 'webp',
                'filters' => [
                    'thumbnail' => [
                        'size' => [$width*2, $height*2],
                        'mode' => $thumbnailMode
                    ]
                ]
            ];
        }
        return $result;
    }
}
