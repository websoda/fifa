<?php declare(strict_types=1);

namespace App\Base\Twig;

use App\Base\Annotation\Twig\TwigFunction as TwigFunctionAnnotation;
use App\Base\Annotation\Twig\TwigTest as TwigTestAnnotation;
use App\Base\Annotation\Twig\TwigFilter as TwigFilterAnnotation;
use Doctrine\Common\Annotations\AnnotationReader;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

abstract class AnnotatedExtension extends AbstractExtension
{
    /**
     * @var
     *
     * [
     *      'App\Base\Annotation\Twig\TwigFunction' => [
     *          'getName' => {instanceOf App\Base\Annotation\Twig\TwigFunction}
     *      ]
     * ]
     */
    protected $map;

    protected function getMethods(string $class): array
    {
        if (!$this->map) {
            $reflection = new \ReflectionClass(static::class);
            $annotationReader = new AnnotationReader();
            foreach ($reflection->getMethods() as $reflectionMethod) {
                $annotations = $annotationReader->getMethodAnnotations($reflectionMethod);
                foreach ($annotations as $annotation) {
                    if ($annotation instanceof TwigFunctionAnnotation) {
                        $this->map[TwigFunctionAnnotation::class][$reflectionMethod->getName()] = $annotation;
                    }
                    if ($annotation instanceof TwigFilterAnnotation) {
                        $this->map[TwigFilterAnnotation::class][$reflectionMethod->getName()] = $annotation;
                    }
                    if ($annotation instanceof TwigTestAnnotation) {
                        $this->map[TwigTestAnnotation::class][$reflectionMethod->getName()] = $annotation;
                    }
                }
            }
        }
        return $this->map[$class] ?? [];
    }

    public function getFunctions(): array
    {
        /** @var TwigFunctionAnnotation[] $annotations */
        $annotations = $this->getMethods(TwigFunctionAnnotation::class);

        $functions = [];
        foreach ($annotations as $method => $annotation) {
            $functions[] = new TwigFunction($annotation->name, [$this, $method], $annotation->options);
        }
        return $functions;
    }

    public function getTests(): array
    {
        /** @var TwigTestAnnotation[] $annotations */
        $annotations = $this->getMethods(TwigTestAnnotation::class);

        $functions = [];
        foreach ($annotations as $method => $annotation) {
            $functions[] = new TwigTest($annotation->name, [$this, $method], $annotation->options);
        }
        return $functions;
    }

    public function getFilters(): array
    {
        /** @var TwigTestAnnotation[] $annotations */
        $annotations = $this->getMethods(TwigFilterAnnotation::class);

        $functions = [];
        foreach ($annotations as $method => $annotation) {
            $functions[] = new TwigFilter($annotation->name, [$this, $method], $annotation->options);
        }
        return $functions;
    }
}
