<?php declare(strict_types=1);

namespace App\Base\Twig;

use App\Base\Annotation\Twig\TwigFunction;
use App\Base\Service\ImageSize;

class ImageSizeExtension extends AnnotatedExtension
{
    /**
     * @var ImageSize
     */
    private $imageSize;

    public function __construct(ImageSize $imageSize)
    {
        $this->imageSize = $imageSize;
    }
    /**
     * @TwigFunction(name="get_image_size_configs")
     * @param string $size
     * @return array
     */
    public function getImageSizeConfigs(string $size): array
    {
        return $this->imageSize->getLiipConfigMap($size);
    }
}
