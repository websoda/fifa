<?php declare(strict_types=1);

namespace App\Base\Annotation\Twig;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
class TwigFilter extends Annotation
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var array
     */
    public $options = [];
}
