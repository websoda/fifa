<?php declare(strict_types=1);

namespace App\Base\Annotation\Orm;

use Doctrine\Common\Annotations\Annotation;

/**
 * Подсказать прямо в Entity что это HTML-поле
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class HtmlEditable extends Annotation
{

}
