<?php declare(strict_types=1);

namespace App\Base\Annotation\Orm;

use Doctrine\Common\Annotations\Annotation;

/**
 * Лейбл для поля
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Label extends Annotation
{
    /**
     * @var string
     */
    public $text;
}
