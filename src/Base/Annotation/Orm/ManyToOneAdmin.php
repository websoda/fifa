<?php declare(strict_types=1);

namespace App\Base\Annotation\Orm;

use Doctrine\Common\Annotations\Annotation;

/**
 * Опции для админки для ManyToOne полей
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class ManyToOneAdmin extends Annotation
{
    /**
     * @var boolean
     */
    public $autocomplete = false;

    /**
     * @var boolean
     */
    public $renderAsNativeWidget = false;
}
