<?php declare(strict_types=1);

namespace App\Base\Annotation\Orm;

use Doctrine\Common\Annotations\Annotation;

/**
 * Help-текст для поля
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class Help extends Annotation
{
    /**
     * @var string
     */
    public $text;
}
