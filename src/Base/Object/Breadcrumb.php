<?php declare(strict_types=1);

namespace App\Base\Object;

class Breadcrumb
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string|null
     */
    private $url;

    public function __construct(string $name, ?string $url)
    {
        $this->name = $name;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }
}