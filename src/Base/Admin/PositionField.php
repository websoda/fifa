<?php declare(strict_types=1);

namespace App\Base\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\FieldTrait;

final class PositionField implements FieldInterface
{
    use FieldTrait;

    public static function new(string $propertyName, ?string $label = null): self
    {
        return (new self())
            ->setProperty($propertyName)
            ->setLabel($label)
            ->setTemplateName('crud/field/position')
            ->setTemplatePath('bundles/EasyAdminBundle/crud/field/position.html.twig')
            ->addCssClass('field-position');
    }
}

