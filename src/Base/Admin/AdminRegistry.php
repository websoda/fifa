<?php declare(strict_types=1);

namespace App\Base\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Registry\CrudControllerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminRegistry
{
    /**
     * @var RelatedSection[]
     */
    protected $relatedRegistry = [];

    public function add(AbstractController $abstractController): void
    {
        if ($abstractController instanceof AdminCrudController) {
            $related = $abstractController->getRelatedAdmins();
            if ($related) {
                $this->relatedRegistry[get_class($abstractController)] = new RelatedSection(
                    $abstractController,
                    $related
                );
            }
        }
    }

    public function getRelatedSection(string $className): ?RelatedSection
    {
        foreach ($this->relatedRegistry as $section) {
            if (get_class($section->getParentAdmin()) === $className) {
                return $section;
            }
            foreach ($section->getRelatedAdmins() as $relatedAdmin) {
                if ($relatedAdmin->getAdminClass() === $className) {
                    return $section;
                }
            }
        }
        return null;
    }

    public function isRelatedAdmin(string $className): bool
    {
        foreach ($this->relatedRegistry as $section) {
            foreach ($section->getRelatedAdmins() as $relatedAdmin) {
                if ($relatedAdmin->getAdminClass() === $className) {
                    return true;
                }
            }
        }
        return false;
    }


    public function findRelatedAdmin(string $className): ?RelatedAdmin
    {
        foreach ($this->relatedRegistry as $section) {
            foreach ($section->getRelatedAdmins() as $relatedAdmin) {
                if ($relatedAdmin->getAdminClass() === $className) {
                    return $relatedAdmin;
                }
            }
        }
        return null;
    }
}
