<?php declare(strict_types=1);

namespace App\Base\Admin;

use App\Base\Service\AdminFieldProvider;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Mapping\Annotation;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\ActionCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\KeyValueStore;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Field\FieldInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\ActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Provider\AdminContextProvider;
use Exception;
use Gedmo\Mapping\Annotation\SortablePosition;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;

abstract class AdminCrudController extends AbstractCrudController
{
    /**
     * @var PropertyInfoExtractorInterface
     */
    private $extractor;
    /**
     * @var ContainerInterface
     */
    protected $commonContainer;
    /**
     * @var AdminRegistry
     */
    private $adminRelatedRegistry;

    /**
     * @var AdminContext
     */
    private $passContext;

    public function __construct(PropertyInfoExtractorInterface $extractor, AdminRegistry $adminRelatedRegistry, ContainerInterface $commonContainer)
    {
        $this->extractor = $extractor;
        $this->commonContainer = $commonContainer;
        $this->adminRelatedRegistry = $adminRelatedRegistry;
    }

    public function getVerboseItemName(): string
    {
        return (new \ReflectionClass(static::getEntityFqcn()))->getShortName();
    }

    public function getVerboseListName(): string
    {
        return (new \ReflectionClass(static::getEntityFqcn()))->getShortName();
    }

    /**
     * @param string $pageName
     * @return iterable
     */
    public function configureFields(string $pageName): iterable
    {
        $entityFqcn = static::getEntityFqcn();
        $fields = $this->getAdminFieldProvider()->getDefaultFields(
            $pageName,
            new $entityFqcn()
        );
        return $this->filterFields($fields);
    }

    /**
     * Оставляем только необходимые поля
     *
     * @param Field[] $fields
     * @return Field[]
     */
    protected function filterFields(array $fields): array
    {
        $relatedAdmin = $this->adminRelatedRegistry->findRelatedAdmin(static::class);
        return array_filter($fields, function(FieldInterface $field) use ($relatedAdmin) {
            // Если данный контроллер - дочерний и поле является свойством через которое связаны объекты, то не отображаем его нигде
            if ($relatedAdmin && $field->getAsDto()->getProperty() === $relatedAdmin->getProperty()) {
                return false;
            }
            return true;
        });
    }

    /**
     * Получить поле, отвечающее за позицию
     *
     * @return string|null
     * @throws \ReflectionException
     */
    protected function getPositionFieldName(): ?string
    {
        $entityFqcn = static::getEntityFqcn();
        $annotationReader = new AnnotationReader();
        $entity = new $entityFqcn();
        $reflectionClass = new ReflectionClass(get_class($entity));
        $reflectionProperties = $reflectionClass->getProperties();
        foreach ($reflectionProperties as $reflectionProperty) {
            $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);
            if ($this->searchAnnotation(SortablePosition::class, $propertyAnnotations)) {
                return $reflectionProperty->getName();
            }
        }
        return null;
    }

    /**
     * Поиск класса аннотаций у объекта
     *
     * @param string $annotationClass
     * @param Annotation[] $propertyAnnotations
     * @return object|null
     */
    protected function searchAnnotation(string $annotationClass, array $propertyAnnotations): ?object
    {
        foreach ($propertyAnnotations as $propertyAnnotation) {
            if (is_a($propertyAnnotation, $annotationClass)) {
                return $propertyAnnotation;
            }
        }
        return null;
    }

    public function configureCrud(Crud $crud): Crud
    {
        $crud = parent::configureCrud($crud);
        $crud->setPageTitle(Crud::PAGE_NEW, $this->getVerboseItemName());
        $crud->setPageTitle(Crud::PAGE_EDIT, $this->getVerboseItemName());
        $crud->setPageTitle(Crud::PAGE_DETAIL, $this->getVerboseItemName());
        $crud->setPageTitle(Crud::PAGE_INDEX, $this->getVerboseListName());
        if ($sortName = $this->getPositionFieldName()) {
            $crud->setDefaultSort([$sortName => 'ASC']);
        }
        return $crud;
    }

    protected function getAdminFieldProvider(): AdminFieldProvider
    {
        return new AdminFieldProvider($this->get(AdminContextProvider::class));
    }

    public function index(AdminContext $context)
    {
        $this->passContext = $context;

        /** @var KeyValueStore $response */
        $response = parent::index($context);
        if ($response instanceof KeyValueStore) {
            $response = $this->fillResponseWithRelatedAdmins($context, $response);
            $response = $this->fillResponseWithCustomAttributes($context, $response);
            $response = $this->renameResponseActions($context, $response);

            /** @var ActionCollection $actions */
            $actions = $response->get('global_actions');
            foreach ($actions->all() as $action) {
                if ($action->getCrudActionName() === 'new' && $response->get('parentId')) {
                    $action->setLinkUrl(
                        $action->getLinkUrl() . '&parentId=' . $response->get('parentId')
                    );
                }
            }
        }
        return $response;
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        $queryBuilder = parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);
        if ($this->adminRelatedRegistry->isRelatedAdmin(static::class)) {
            $relatedMenu = $this->adminRelatedRegistry->getRelatedSection(static::class);
            $relatedAdmin = $relatedMenu->findRelatedAdminByClass(static::class);
            $parentId = $this->passContext->getRequest()->get('parentId');
            if (!$parentId) {
                throw new Exception('Unknown parentId for related admin');
            }
            $fieldName = $relatedAdmin->getProperty();
            $queryBuilder
                ->where("entity.{$fieldName} = :parentId")
                ->setParameter('parentId', $parentId);
        }
        return $queryBuilder;
    }

    public function edit(AdminContext $context)
    {
        /** @var KeyValueStore $response */
        $response = parent::edit($context);
        if ($response instanceof KeyValueStore) {
            $response = $this->renameResponseActions($context, $response);
            $response = $this->fillResponseWithRelatedAdmins($context, $response);
            $response = $this->fillResponseWithCustomAttributes($context, $response);
        }
        return $response;
    }

    public function createEntity(string $entityFqcn)
    {
        $entity = parent::createEntity($entityFqcn);
        if (!$this->adminRelatedRegistry->isRelatedAdmin(static::class)) {
            return $entity;
        }
        $relatedMenu = $this->adminRelatedRegistry->getRelatedSection(static::class);
        $relatedAdmin = $relatedMenu->findRelatedAdminByClass(static::class);
        $parentId = $this->passContext->getRequest()->get('parentId');
        if (!$parentId) {
            throw new Exception('Unknown parentId for related admin');
        }
        $parentEntity = $this->getDoctrine()->getRepository(
            $relatedMenu->getParentAdmin()->getEntityFqcn()
        )->find($parentId);
        if (!$parentEntity) {
            throw new Exception('Unknown parentEntity for related admin');
        }
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $propertyAccessor->setValue($entity, $relatedAdmin->getProperty(), $parentEntity);
        return $entity;
    }

    public function new(AdminContext $context)
    {
        $this->passContext = $context;
        /** @var KeyValueStore $response */
        $response = parent::new($context);
        if ($response instanceof KeyValueStore) {
            $response = $this->renameResponseActions($context, $response);
            $response = $this->fillResponseWithRelatedAdmins($context, $response);
            $response = $this->fillResponseWithCustomAttributes($context, $response);
        }
        return $response;
    }

    protected function renameResponseActions(AdminContext $context, KeyValueStore $response): KeyValueStore
    {
        $names = $this->getActionsNames();
        $globalActions = $response->get('global_actions');
        if ($globalActions) {
            /** @var ActionDto $action */
            foreach ($globalActions->all() as $action) {
                $name = $names[$action->getCrudActionName()] ?? null;
                if (!$name) {
                    continue;
                }
                $action->setLabel('Добавить');
            }
        }
        return $response;
    }

    protected function getActionsNames()
    {
        return [
            'new' => 'Создать'
        ];
    }

    protected function fillResponseWithCustomAttributes(AdminContext $context, KeyValueStore $response): KeyValueStore
    {
        $response->set('isAdminCrud', true);
        $response->set('currentAdminController', $this);
        return $response;
    }

    protected function fillResponseWithRelatedAdmins(AdminContext $context, KeyValueStore $response): KeyValueStore
    {
        $relatedMenu = $this->adminRelatedRegistry->getRelatedSection(static::class);
        $response->set('relatedMenu', $relatedMenu);
        $response->set('relatedActive', static::class);
        $parentId = $context->getRequest()->get('parentId');
        $response->set('parentId', $parentId);
        $parentEntity = null;
        if ($relatedMenu) {
            $response->set('isRelated', static::class === $relatedMenu->getParentAdminClass());
            if ($parentId) {
                $parentAdmin = $relatedMenu->getParentAdmin();
                $entityFqcn = $parentAdmin::getEntityFqcn();

                $parentEntity = $this->getDoctrine()->getRepository($entityFqcn)->find($parentId);
                $response->set('relatedParentEntity', $parentEntity);
            }
        }
        return $response;
    }

    public function getRelatedAdmins(): array
    {
        return [];
    }

    protected function containsRelatedAdmin(string $class): bool
    {
        foreach ($this->getRelatedAdmins() as $key => $value) {
            return $class === $key;
        }
        return false;
    }
}
