<?php declare(strict_types=1);

namespace App\Base\Admin;

class RelatedAdmin
{
    /**
     * @var string
     */
    private $adminClass;
    /**
     * @var string|null
     */
    private $title;
    /**
     * @var string|null
     */
    private $property;

    public function __construct(string $adminClass, string $title = null, string $property = null)
    {
        $this->adminClass = $adminClass;
        $this->title = $title;
        $this->property = $property;
    }

    /**
     * @return string
     */
    public function getAdminClass(): string
    {
        return $this->adminClass;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title ?: (new \ReflectionClass($this->getAdminClass()))->getShortName();
    }

    /**
     * @return string|null
     */
    public function getProperty(): ?string
    {
        return $this->property;
    }
}
