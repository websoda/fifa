<?php declare(strict_types=1);

namespace App\Base\Admin;

class RelatedSection
{
    /**
     * @var AdminCrudController
     */
    private $parentAdmin;
    /**
     * @var RelatedAdmin[]
     */
    private $relatedAdmins;

    /**
     * RelatedSection constructor.
     * @param AdminCrudController $parentAdmin
     * @param RelatedAdmin[] $relatedAdmins
     */
    public function __construct(AdminCrudController $parentAdmin, array $relatedAdmins)
    {
        $this->parentAdmin = $parentAdmin;
        $this->relatedAdmins = $relatedAdmins;
    }

    /**
     * @return AdminCrudController
     */
    public function getParentAdmin(): AdminCrudController
    {
        return $this->parentAdmin;
    }

    public function getParentAdminClass(): string
    {
        return get_class($this->parentAdmin);
    }

    /**
     * @return RelatedAdmin[]
     */
    public function getRelatedAdmins(): array
    {
        return $this->relatedAdmins;
    }

    public function findRelatedAdminByClass(string $className): ?RelatedAdmin
    {
        foreach ($this->relatedAdmins as $relatedAdmin) {
            if ($relatedAdmin->getAdminClass() === $className) {
                return $relatedAdmin;
            }
        }
        return null;
    }
}
