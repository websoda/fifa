/**
 * Слушать событие готовности DOM-дерева после загрузки или изменений
 *
 * @param callback
 */
function onDomReady(callback) {
  if (document.readyState === "complete"
    || document.readyState === "loaded"
    || document.readyState === "interactive") {
    callback();
  } else {
    document.addEventListener('DOMContentLoaded', callback);
  }
  document.addEventListener('DOMContentMutated', callback);
}

export {onDomReady};