import {scrollToTarget} from "../../components/scroll/scroll";
import {singleBind} from "../../components/dynamic/bind";
import {onDomReady} from "../../components/dynamic/observer";

function onClickScroll(e) {
  e.preventDefault();
  let target = null;
  if (this.dataset.selector) {
    target = document.querySelector(this.dataset.selector);
  } else {
    target = document.querySelector(this.getAttribute('href'));
  }
  let offset = parseInt(this.dataset.offset) || 0;
  if (target) {
    scrollToTarget(target, offset);
  }
}

function onClickToggle(e) {
  e.preventDefault();
  const target = document.querySelector(this.dataset.selector);
  let toggleClass = this.dataset.toggle;
  if (!toggleClass && this.getAttribute('href')) {
    toggleClass = this.getAttribute('href');
  }
  target.classList.toggle(toggleClass);
}

onDomReady(() => {
  document.querySelectorAll('[data-scroll-link], .scroll-link').forEach((element) => {
    singleBind(element, 'initialized', 'click', onClickScroll);
  });
  document.querySelectorAll('[data-toggle-link], .toggle-link').forEach((element) => {
    singleBind(element, 'initialized', 'click', onClickToggle);
  });
});