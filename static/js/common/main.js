import Modal from "../../components/modal/modal";
import {onDomReady} from "../../components/dynamic/observer";
import {singleBind} from "../../components/dynamic/bind";

function openModal(e) {
  e.preventDefault();
  let link = this;
  let modal = new Modal(this, {
    closerText: '',
    onFormSuccess: function () {
      if (link.dataset.goal) {
        let action = 'click';
        if(link.dataset.goalAction) {
          action = link.dataset.goalAction;
        }
        window.goal(link.dataset.goal, action);
      }
    },
  });
  return false;
}

onDomReady(() => {
  document.querySelectorAll('[data-modal]').forEach((link) => {
    singleBind(link, 'initialized', 'click', openModal);
  });
});