import Vue from 'vue';

import App from './App.vue';

if (document.querySelector('#app')) {
  const vm = new Vue({
    el: '#app',
    render: h => h(App),
  });
}