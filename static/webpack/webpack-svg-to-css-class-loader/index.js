const path = require('path');
const svgson = require('svgson');

module.exports = function load(source) {
  const callback = this.async();
  const fileName = path.basename(this.resourcePath, '.svg');

  svgson(source, {}, (result) => {
    let dims = [0, 0, 0, 0];
    if (result.attrs.viewBox) {
      dims = result.attrs.viewBox.split(/\s/);
      if (dims.length !== 4) {
        dims = [0, 0, 0, 0];
      }
    } else if (result.attrs.width && result.attrs.height) {
      dims = [0, 0, parseInt(result.attrs.width, 10), parseInt(result.attrs.height, 10)];
    }
    const d = {
      top: dims[0],
      left: dims[1],
      width: dims[2],
      height: dims[3],
    };
    callback(null, `.icon-${fileName} { width: ${d.width}px; height: ${d.height}px; }`);
  });
};