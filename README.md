# Шаблон проекта на Symfony в Docker

## Полная информация о том как что к чему


Читаем подробно - гайд по разработке: [Гайд](docs/GUIDE.md)

    ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

## Разворачивание проекта

Клонируем проект

Далее, создаем файлик .env.local
В самом .env ничего не меняем!

.env.local

```
DATABASE_URL=mysql://USER:PASSWORD@mysql:3306/DATABASENAME?serverVersion=8.1.0&charset=utf8mb4
APP_SECRET=APP_SECRET
```


Собираем контейнеры:

```
make dc-build
make dc-docker-start
```

Подключаемся к контейнеру c php:
```
make app_bash
```

Далее, выполняем:

```
bin/console assets:install
bin/console ckeditor:install
bin/console elfinder:install
bin/console doctrine:database:create
```

#####Вход на сайт
```
127.0.0.1:888
```

## Поддержка twig плюшек в среде
Ставим плагины:
- Symfony Support
- Twig Support
- PHP Annotation

Включаем в настройках:
- Preferences... -> Language & Frameworks -> PHP -> Symfony
- Enable plugin for this project
- Перезагружаем среду

## Кратенько команды

оф документация: https://symfony.com/doc/current/index.html

Команды:

Создать пользователя (email, password):

```
bin/console user:create:admin admin@admin.com admin
```


Создать миграции (после изменения в бд) и применить их.
После изменений в Entity.

```
bin/console doctrine:migrations:diff
bin/console doctrine:migrations:migrate --no-interaction
```

Просто применить миграции (после pull).

```
bin/console doctrine:migrations:migrate --no-interaction
```

Создать CRUD-controller для админки:

```
bin/console make:admin:crud
```

Добавить Entity

```
bin/console make:entity
```

Опубликовать ассеты из бандлов:

```
bin/console assets:install
```