# Руководство по разработке проектов на нашем шаблоне Symfony-проекта

## Что это и для чего?

Это структура, некоторое соглашение об именовании и дополнения к EasyAdmin (админке Symfony) и частям Symfony.
## Структура проекта

Код лежит в папке `src` следующим образом (на примере контроллеров) - общая папка со всеми контроллерами, 
внутри нее папки по группам сущностей (назовем их условно модулями), внутри папки модуля - непосредственно контроллеры.

Такая структура распространяется на:
- Admin
- Command
- Controller
- Entity
- Form
- Repository
- Twig

## Создание сущностей и репозиториев

Создание сущностей производим с учетом структуры проекта.

Допустим, нам нужно создать сущность `Article` в модуле `Articles`

Вводим команду создания сущностей (именно так, с двумя слешами). 

```bash
bin/console make:entity Articles\\Article
```

Пользуемся командами, не боимся, она подсказывает что и как вводить. Главное читаем внимательно.

## Синхронизация базы

Для простых проектов мы не будем выдумывать какую-то сложную логику с миграциями и просто будем синхронизировать базу с кодом:

```bash
bin/console doctrine:schema:update -f
```

## Админка

Чтобы добавить сущность (возьмем за пример сущность `GameReview`) в админку нужно:

- Создать папку в src/Admin по имени модуля (если такой еще нет). Например, Games
- Создать класс `GameReviewCrudController`
- Унаследовать его от [App\Base\Admin\AdminCrudController](../src/Base/Admin/AdminCrudController.php) (это наша надстройка над EasyAdmin)
- Указать в нем класс сущности (метод `getEntityFqcn`) и указать его имя (в единственном числе - `getVerboseItemName` / множественном числе - `getVerboseListName`)
- Добавить пункт меню в [DashboardController](../src/Admin/Common/DashboardController.php) - в методе `configureMenuItems`

EasyAdmin в 90% случаев требует указания полей для сущностей вручную, наша надстройка позволяет это делать в крайне редких ситуациях.

### Указание Label и Help в админке

Для указания, добавляем аннотации к свойству сущности. Например:


```php
...
use App\Base\Annotation\Orm\Help;
use App\Base\Annotation\Orm\Label;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class GameReview
{
    ...

    /**
     * @ORM\Column(type="string", length=255)
     * @Label(text="Текст отзыва")
     * @Help(text="Не более 20 слов")
     */
    private $text;

    ...
}
```

### ManyToOneAdmin

В админке ManyToOne связь по-умолчанию рендерится как Select2-поле.

Аннотация `ManyToOneAdmin` позволяет для ManyToOne связи указать следующие настройки:
- `autocomplete` - опция позволяет не выводить в список все выбираемые модели, а искать их через ajax.
    - плюсы: если у нас список выбираемых моделей более тысячи штук, ничего не будет тормозить, так как пользователь не видит сразу все варианты выбора, а только их ограниченное количество
    - минусы: нужно ввести минимум 1 символ в поле поиска чтобы увидеть варианты выбора
- `renderAsNativeWidget` - отображать поле как обычный select, без использования Select2

Пример включенного autocomplete:

```php
...
use App\Base\Annotation\Orm\Help;
use App\Base\Annotation\Orm\Label;
use App\Base\Annotation\Orm\ManyToOneAdmin;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class GameReview
{
    ...

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="gameReviews")
     * @ManyToOneAdmin(autocomplete=true)
     * @ORM\JoinColumn(nullable=false)
     * @Label(text="Игра")
     * @Help(text="Текст для помощи")
     */
    private $game;

    ...
}
```

### HTML-редактор HtmlEditable

Включить в админке отображение WYSIWYG-редактора можно с помощью аннотации `HtmlEditable`.

Пример:

```php
...
use App\Base\Annotation\Orm\HtmlEditable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Game
{
    ...

    /**
     * @ORM\Column(type="text", nullable=true)
     * @HtmlEditable()
     */
    private $text;

    ...
}
```

### Загрузка файлов и изображений

Самые обычные настройки для `VichUploader`, ничего особо делать не надо.

Единственный момент, если в названии поля есть слово "*image*", то в админке оно будет отображаться с предпросмотром

Пример для поля `image` в модели `Game`:

```php
...
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

// Важно! @Vich\Uploadable() необходимо указать у сущности
/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 * @Vich\Uploadable()
 */
class Game
{
    ...
    /**
     * NOTE: Это не Column, а просто простое свойство. Для работы загрузки файлов.
     * Информация о размерах изображений хранится в config/image_sizes.yaml
     *
     * Тут указываем в fileNameProperty имя свойства в котором будет храниться путь до изображения
     * @Vich\UploadableField(mapping="uploads", fileNameProperty="image")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $image;

    /**
     * Необходимо для загрузки картинок
     *
     * @ORM\Column(type="datetime")
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     * @throws \Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;
        if (null !== $imageFile) {
            $this->updatedAt = new DateTimeImmutable();
        }
    }
    
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

}
```

#### Вывод изображений на морду

Для вывода адаптивного изображения нам потребуется:
1. Указать нужные размеры изображений для каждого разрешения
2. Вывести их на морду

Указываем нужные размеры изображений в файле `config/image_sizes.yaml`
После изменения файла не забудьте почистить кеш

Допустим, для превью изображения сущности `Game` нам потребуется следующий набор размеров:

```yaml
sizes:
  game_thumb:
    small:
      width: 200
      height: 300
      size: cover
    medium:
      width: 300
      height: 400
      size: cover
    large:
      width: 500
      height: 600
      size: cover
```

Настраиваем (один раз за проект размеры разрешений в файле [adaptive-image.html.twig](../templates/_parts/image/adaptive-image.html.twig))

Выводим изображение (пример для Game):

```twig
{% include '_parts/image/adaptive-image.html.twig' with {
    object: game,
    field: 'imageFile',
    size: 'game_thumb',
    alt: 'ЗАМЕНИТЕ АЛЬТ',
    class: 'КЛАСС IMG',
    classPicture: 'КЛАСС PICTURE',
} %}
```

    Обязательно! Укажите корректный alt и при необходимости классы для picture и img


### Поле позиции (сортировки)

Аннотация `SortablePosition` - в админке будет выведено как возможность перетаскивать их местами.
Пример:

```php
...
use Gedmo\Sortable\Sortable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity()
 */
class GameLevel implements Sortable
{
    ...
    /**
     * @var int|null $position
     *
     * @Gedmo\SortablePosition()
     * @ORM\Column(name="position", type="integer")
     */
    private $position;
    ...
}
```


### Связанные Админки

Ограничение - поддерживается только один уровень вложеннсти (по крайней мере пока).

Описываем две обычные админки, дочернюю НЕ добавляем в меню в DashboardController (ибо зачем)

В основной указываем список связанных админок в методе ```getRelatedAdmins``.

Пример:
- Основная модель `Game`, админка `GameCrudController`
- Дочерняя модель `GameLevel`, свойство для ManyToOne до `Game` - `game`, админка `GameLevelCrudController`

`GameCrudController`:

```php
namespace App\Admin\Games;

use App\Base\Admin\AdminCrudController;
use App\Base\Admin\RelatedAdmin;
use App\Entity\Games\Game;

class GameCrudController extends AdminCrudController
{
    public static function getEntityFqcn(): string
    {
        return Game::class;
    }

    public function getVerboseItemName(): string
    {
        return 'Игра';
    }

    public function getVerboseListName(): string
    {
        return 'Игры';
    }

    public function getRelatedAdmins(): array
    {
        return [
            new RelatedAdmin(
                GameLevelCrudController::class,
                'Уровни',
                'game'
            )
        ];
    }
}
```

### Расширения для Twig

Наследуем наш Extension от `App\Base\Twig\AnnotatedExtension`
Используем аннотации.

#### Добавить функцию в Twig

```php

<?php declare(strict_types=1);

namespace App\Twig\Main;

use App\Base\Annotation\Twig\TwigFunction;
use App\Base\Object\Breadcrumb;
use App\Base\Twig\AnnotatedExtension;
use App\Base\Service\Breadcrumbs;

class BaseExtension extends AnnotatedExtension
{
    ...
    /**
     * @TwigFunction(name="get_breadcrumbs")
     * @param string $list
     * @return Breadcrumb[]
     */
    public function getBreadcrumbs(string $list = Breadcrumbs::DEFAULT_LIST)
    {
        return $this->breadcrumbs->all($list);
    }
    ...
}
```


#### Добавить фильтр в Twig

```php

<?php declare(strict_types=1);

namespace App\Twig\Main;

use App\Base\Annotation\Twig\TwigFilter;
use App\Base\Twig\AnnotatedExtension;

class BaseExtension extends AnnotatedExtension
{
    ...
    /**
     * @TwigFilter(name="tel")
     * @param $phone
     * @return string
     */
    public function tel($phone): string
    {
        $phone = (string) $phone;
        $phone = preg_replace('/[^0-9\+]/','', $phone);
        if (mb_substr($phone, 1, 1, 'UTF-8') === '8') {
            $phone = '+7' . mb_substr($phone, 1, mb_strlen($phone, 'UTF-8') - 1, 'UTF-8');
        }
        return $phone;
    }
    ...
}
```

