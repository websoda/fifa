const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = (env) => {
  const plugins = [];
  if (env.analyze) {
    const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
    plugins.push(new BundleAnalyzerPlugin());
  }
  return merge(common(env), {
    mode: 'production',
    plugins,
  });
};