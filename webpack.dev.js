const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = env => (merge(common(env), {
  mode: 'development',
  // devtool: 'inline-source-map',
  devServer: {
    host: '0.0.0.0',
    port: 9000,
    hot: true,
    inline: true,
    contentBase: './',
    proxy: {
      '*': {
        target: 'http://127.0.0.1:888/',
        changeOrigin: true,
        headers: {
          'X-Forwarded-Port': '9000',
          'X-Forwarded-Host': '127.0.0.1'
        }
      },
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization, x-id, Content-Length, X-Requested-With',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS'
    },
  },
}));