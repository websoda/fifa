##################
# Variables
##################

DOCKER_COMPOSE = docker-compose -f ./docker/docker-compose.yml
DOCKER_COMPOSE_PROD = docker-compose -f ./docker/docker-compose.prod.yml

##################
# Docker compose
##################

dc-build:
	${DOCKER_COMPOSE} build

dc-start:
	${DOCKER_COMPOSE} start

dc-stop:
	${DOCKER_COMPOSE} stop

dc-docker-start:
	${DOCKER_COMPOSE} -p my-service up --build

dc-docker-stop:
	${DOCKER_COMPOSE} -p my-service down -v

dc-docker-start-prod:
	${DOCKER_COMPOSE_PROD} -p my-service up -d --build

dc-docker-stop-prod:
	${DOCKER_COMPOSE_PROD} -p my-service down -v

dc-up:
	${DOCKER_COMPOSE} up -d --remove-orphans

dc-ps:
	${DOCKER_COMPOSE} ps

dc-logs:
	${DOCKER_COMPOSE} logs -f

dc-down:
	${DOCKER_COMPOSE} down -v --rmi=all --remove-orphans

dc-restart:
	make dc-stop dc-start


##################
# App
##################

app_bash:
	${DOCKER_COMPOSE} -p my-service exec php-fpm bash

app_zsh:
	${DOCKER_COMPOSE} -p my-service exec php-fpm zsh

mysql_bash:
	${DOCKER_COMPOSE} -p my-service exec mysql bash

bem:
	${DOCKER_COMPOSE} -p my-service exec -T php-fpm yarn bem

migration:
	${DOCKER_COMPOSE} -p my-service exec -T php-fpm php bin/console make:migration

migration_diff:
	${DOCKER_COMPOSE} -p my-service exec -T php-fpm php bin/console doctrine:migrations:diff --no-interaction

migrate:
	${DOCKER_COMPOSE} -p my-service exec -T php-fpm php bin/console doctrine:migrations:migrate --no-interaction

cache:
	${DOCKER_COMPOSE} -p my-service exec -T php-fpm php bin/console cache:clear && chmod -R 777 var/cache/prod

admin_crud:
	${DOCKER_COMPOSE} -p my-service exec -T php-fpm bin/console make:admin:crud