const path = require('path');

const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const SvgSpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const WebPInCss = require('webp-in-css/plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const ManifestPlugin = require('./static/webpack/webpack-manifest-plugin');

const postcssOptions = {
  plugins: [
    autoprefixer(),
    cssnano(),
    WebPInCss()
  ],
  sourceMap: true,
};

module.exports = env => ({
  entry: path.resolve('static/js/app.js'),
  devtool: 'source-map',
  output: {
    path: path.join(__dirname, 'public/static'),
    filename: '[name]-[hash].js',
    publicPath: '/static/',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
          plugins: ['@babel/plugin-proposal-class-properties']
        }
      }],
    },
    {
      test: /\.vue$/,
      exclude: /node_modules/,
      use: [{
        loader: 'vue-loader',
        options: {
          loaders: {
            scss: [
              'vue-style-loader',
              'css-loader',
              'sass-loader',
            ],
          },
        },
      }],
    },
    {
      test: /\.(png|jpe?g|webp)$/,
      use: [{
        loader: 'file-loader',
        options: {
          name: '[name]-[hash].[ext]',
          outputPath: 'images-processed',
        },
      }],
    },
    {
      test: /\.(otf|ttf|eot|woff|woff2)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            name: '[name]-[hash].[ext]',
            outputPath: 'fonts',
          },
        },
      ],
    },
    {
      test: /\.css$/,
      use: [
        env.production ? {
          loader: MiniCssExtractPlugin.loader,
        } : {
          loader: 'style-loader'
        },
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
          },
        },
        {
          loader: 'postcss-loader',
          options: postcssOptions,
        },
      ],
    },
    {
      test: /\.scss$/,
      use: [
        env.production ? {
          loader: MiniCssExtractPlugin.loader,
        } : {
          loader: 'style-loader'
        },
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
          },
        },
        {
          loader: 'postcss-loader',
          options: postcssOptions,
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            includePaths: [path.resolve(__dirname, 'static/scss/_settings')],
          },
        },
      ],
    },
    {
      test: /\.svg$/,
      include: path.resolve(__dirname, 'static/svg-css'),
      use: [
        env.production ? {
          loader: MiniCssExtractPlugin.loader,
        } : {
          loader: 'style-loader'
        },
        {
          loader: 'css-loader',
        },
        {
          loader: 'postcss-loader',
          options: postcssOptions,
        },
        {
          loader: './static/webpack/webpack-svg-to-css-class-loader/index.js',
        },
      ],
    },
    {
      test: /\.svg$/,
      include: path.resolve(__dirname, 'static/svg-sprite'),
      use: [
        {
          loader: 'svg-sprite-loader',
          options: {
            spriteFilename: 'sprite-[hash].svg',
            extract: true,
            esModule: false,
          },
        },
        {
          loader: 'svg-transform-loader',
        },
        {
          loader: 'svgo-loader',
        },
      ],
    },
    {
      test: /\.svg$/,
      issuer: /\.s?css$/,
      use: [{
        loader: 'file-loader',
        options: {
          name: '[name]-[hash].[ext]',
          outputPath: 'svg',
        },
      }],
    },
    ],
  },
  plugins: [
    new webpack.ProgressPlugin(),
    new VueLoaderPlugin(),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([{
      from: path.resolve(__dirname, 'static/images'),
      to: 'images',
    }]),
    new MiniCssExtractPlugin({
      filename: '[name]-[hash].css',
      chunkFilename: '[id].css',
    }),
    new ManifestPlugin({
      writeToFileEmit: true,
      map: (file) => {
        // Fix svg sprite name
        if (file.name.toLowerCase()
          .endsWith('.svg')) {
          file.name = file.name.replace(/(?=(sprite))(.*)(?=\.svg$)/, '$1');
        }
        return file;
      },
      seed: {}
    }),
    new SvgSpriteLoaderPlugin({
      plainSprite: true,
    }),
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: (item) => {
            if (item.resource && item.resource.match(/[\\/]node_modules[\\/]/)) {
              return !item.resource.match(/\.s?css$/);
            }
            return false;
          },
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },
});